Coded in Java. This is a my final project for Software Design Lab in Fall 2014. 
I created a company database with a user interface that allows employees to check 
in and check out. It also allows the employer to view employee information, and
add or remove employees. This project demonstrates concepts such as inheritance,
polymorphism, and graphical user interfaces. 